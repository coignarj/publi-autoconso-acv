{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Input data\n",
    "Information about the input data for the article \"Including greenhouse gas emissions and behavioural responses for PV self-sufficient  optimal design\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data sources\n",
    "The following data sources  were employed, with time steps of 15 minutes over a one year horizon:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Consumption data\n",
    "Consumption data comes from 20 selected French households from [Quoilin et al. database](https://github.com/squoilin/Self-Consumption/releases) licensed under the “European Union Public Licence\" EUPL v1.1. It is used in the article S. Quoilin, K. Kavvadias, A. Mercier, I. Pappone, A. Zucker, Quantifying self-consumption linked to solar home battery systems: statistical analysis and economic assessment, Applied Energy, 2016. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### PV production data\n",
    "PV production data are determined with [PVGIS](https://ec.europa.eu/jrc/en/pvgis), an online tool developed by the European Commission Joint Research Centre (doi: https://doi.org/10.1016/j.solener.2012.03.006). The chosen locations  are Grenoble (France) and Berlin (Germany). The data are free for public use if the source is acknowledged.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### GHG emissions data\n",
    "GHG emissions data from the French power grid come from the French Transmission System Operator RTE database [eco2mix](https://www.rte-france.com/eco2mix/telecharger-les-indicateurs) with the open data license etalab 2.0. They have been updated with the IPCC ratios in order to take into account the life cycle GHG emissions. A similar method has been used for German power grid emissions.  \n",
    "#### Conversion factors by fuel types  \n",
    "\n",
    "Fuel types|CF [gCO2/kWh]|Source\n",
    "-----|-----|------\n",
    "Fuel|650|UK POST 2014\n",
    "Coal|820|IPCC 2014\n",
    "Gas|490|IPCC 2014\n",
    "Nuclear|12|IPCC 2014\n",
    "Wind|11|IPCC 2014\n",
    "Solar|45|IPCC 2014\n",
    "Hydro|24|IPCC 2014\n",
    "Bioenergy|230|IPCC 2014\n",
    "\n",
    "#### Conversion factors from energy imports by country\n",
    "\n",
    "Country|CF [gCO2/kWh]|Source\n",
    "-----|-----|------\n",
    "England|591|[1]\n",
    "Spain|312|[1]\n",
    "Italy|490|[1]\n",
    "Germany|574|[1]\n",
    "Switzerland|206|[2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The carbon intensity is calculated as follow:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "E_{total}(t) = \\sum^{Source}_{s} {E(t,s)}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\n",
    "ECI(t) = \\sum^{Source}_{s} \\frac{E(t,s)}{E_{total}(t)}.CF(s)\n",
    "$$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Where:\n",
    " - $ECI$ is electricity carbon intensity of the electrical grid, in gCO2.\n",
    " - $E$ is energy consumption as a function of time (t) and its source (s), in kWh.\n",
    " - $CF$ is conversion factor as a function of the energy source source (s), in gCO2-eq/kWh."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "References:   \n",
    "    [1] A. Moro and L. Lonza, ‘Electricity carbon intensity in European Member States: Impacts on GHG emissions of electric vehicles’, Transportation Research Part D: Transport and Environment, vol. 64, pp. 5–14, Oct. 2018, doi: 10.1016/j.trd.2017.07.012.\\\\    \n",
    "    [2] D. Vuarnoz and T. Jusselme, ‘Temporal variations in the primary energy use and greenhouse gas emissions of electricity provided by the Swiss grid’, Energy, vol. 161, pp. 573–582, Oct. 2018, doi: 10.1016/j.energy.2018.07.087.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### GWP data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Regarding the **GWP values for PV panels**, given  the high uncertainties involved, the results will be presented with minimal, median and maximal GWP values, offering an envelope rather than a single curve. The minimal, median and maximal Global Warming Potentials (GWP) of PV panels (respectively 858, 1040 and 1240 kgCO2/kWp) come from the [INCER-ACV](viewer.webservice-energy.org/incer-acv/app/) project funded by the French Agency for ecological transition ([ADEME](https://www.ademe.fr/en)). These are based on ecoinvent databases using open source libraries Birghtway2 and LCA_Algerbraic.  Minimal and maximal values respectively represent the 10th and 90th percentiles of the INCER-ACV data.\n",
    "\n",
    "The **GWP of the Li-ion batteries** is considered equal to 158 kgCO2 eq./kWh of battery capacity, considering LFP technology (Peters, J.F., 2017. The environmental impact of Li-Ion batteries and the role of key parameters – A review. Renewable and Sustainable Energy Reviews 16.). Associated  lifespan of 8 years  is approximated, considering one cycle per day. The maximal Depth of Discharge (DoD) is considered equal to 80%. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Energy sufficiency\n",
    "Finally, the assumption of energy sufficiency is based on the thermal and appliances consumption in the Global North from the Low Energy Demand scenario (doi: https://doi.org/10.1038/s41560-018-0172-6), with an annual decrease of 3% over the 20-years horizon. It is calculated based on the decrease  of both the thermal consumption (75%) and the appliances consumption (35%) per household over 30 years.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The yearly evolution of the consumption is about -3%\n"
     ]
    }
   ],
   "source": [
    "# Consumer goods per capita\n",
    "consumption_goods_2020 = 3490  #kWh\n",
    "consumption_goods_2050 = 2277  #kWh\n",
    "decrease_cons_goods = (consumption_goods_2020 - consumption_goods_2050)/consumption_goods_2020\n",
    "\n",
    "# Thermal consumption per capita\n",
    "thermal_cons_2020 = 5560  #kWh\n",
    "thermal_cons_2050 = 1390  #kWh\n",
    "decrease_thermal = (thermal_cons_2020 - thermal_cons_2050)/thermal_cons_2020\n",
    "\n",
    "# Total decrease percentage, weighted by the consumptions\n",
    "total_decrease_30y = (thermal_cons_2020*decrease_thermal + decrease_cons_goods * consumption_goods_2020)/(thermal_cons_2020 + consumption_goods_2020)\n",
    "\n",
    "# (1+yearly_evolution)^n=(1-total_decrease_30y) ==>\n",
    "yearly_evolution = (1-total_decrease_30y)**(1/30)-1\n",
    "print(\"The yearly evolution of the consumption is about \" + str(round(yearly_decrease*100))+\"%\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The reference case consists in no change in the energy consumption: it is obviously not realistic but enables intuitive comparisons."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
