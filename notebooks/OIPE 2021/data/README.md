# Data folder
Do not commit data directly, but rather provide a link to an external source.

## Residential consumption data
https://github.com/squoilin/Self-Consumption/releases, S. Quoilin, K. Kavvadias, A. Mercier, I. Pappone, A. Zucker, Quantifying self-consumption linked to solar home battery systems: statistical analysis and economic assessment, Applied Energy, 2016, licensed under the “European Union Public Licence" EUPL v1.1
## PV production data
https://re.jrc.ec.europa.eu/pvg_tools/en/#MR, PVGIS, free to use data.

## C02 emission data
France: https://www.rte-france.com/en/eco2mix/co2-emissions licensed with the open data license etalab 2.0.
Germany: electricity map, free to use for academic purpose.
