import setuptools
from setuptools import setup

with open("requirements.txt", "r") as f:
    requirements = f.read().split()

setup(
    name="co2sizing",
    version="0.0.1",
    description="Sizing optimization for self-consumption",
    long_description_content_type="text/markdown",
    url="none",
    author="People",
    author_email=["email@gmail.com"],
    keywords="Optimization, CO2, PV, battery",
    packages=setuptools.find_packages(),
    # install_requires=requirements,
    python_requires=">=3.8",
)
