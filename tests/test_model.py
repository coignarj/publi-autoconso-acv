import pytest
import os
import pandas as pd
from co2sizing import models
from co2sizing import utils


@pytest.fixture(scope="session")
def result():
    file = os.path.join(os.path.dirname(__file__), "../notebooks/OIPE 2021/data/clustered_data_FR.pickle")
    clusters = pd.read_pickle(file)

    data = []
    for index, frame in clusters.groupby("Cluster_number"):
        data.append(frame.copy())

    inputs = {
        "pv_gwp": 2460,  # kgCo2/kW
        "battery_gwp": 132,  # kgCO2/kWh, we change batteries 6 times within 20 years.
        "self_sufficiency": 0.7,
        "period_coefs": [11.6, 12.09, 12.08, 11.87, 4.41],
        "battery_eta": 0.95,
        "soh_coefs": [1.0 * 0.93 ** n for n in range(8)],
        "pv_ageing_coefs": [1 - 0.005 * i for i in range(20)],
    }
    inputs["positiv_coefs"] = [1.0 * 0.965 ** n for n in range(0, 20)]

    model = models.base.optimize(data, **inputs, verbose=True, solver="gurobi")
    result = utils.base.post_process(model, data, inputs["period_coefs"], yearly_cons_mwh=20, verbose=False)
    return result


def test_optimization_selfsuff(result):
    assert result["self_sufficiency"] == 70.0


def test_optimization_totalco2(result):
    assert result["co2_kg_mwh"] == 740.3  # kgCO2eq/MWh


def test_optimization_pvcap(result):
    assert result["pv_capacity"] == 100.15 # kWp


def test_optimization_batterycap(result):
    assert result["battery_kwh"] == [266.53, 196.01, 135.88]  # kWh


def test_optimization_capex(result):
    assert result["capex"] == 329490.96  # kgCO2eq


def test_optimization_soh(result):
    assert result["battery_cap"][0] - result["battery_cap"][1] == pytest.approx(
        result["battery_cap"][0] * (1 - 0.93), abs=1e-6
    )
