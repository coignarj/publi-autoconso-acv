# GHG energy systems sizing

Abstract: **Purpose** - This paper aims at considering both the greenhouse gas (GHG) emissions and behavioural response in the sizing optimisation of a solar photovoltaic system (PV modules and batteries). The objective is to achieve a high self-sufficiency rate whilst taking into account the grid carbon intensity and the Global Warming Potential of the individual components of the system. **Design/methodology/approach** - The optimisation problem requires a 15 min granularity over 20 years. We first apply a simulation periods reduction using a clustering approach, before solving a linear programming approach to compute the optimisation in a reasonable time. **Findings** – Our results show that the minimum GHG emissions is achieved for self-sufficiency rates of 19% in France and 50% in Germany. **Research limitations/implications** – Our analysis is restricted to specific residential profile: further work will focus on exploring different types of consumption profile. **Practical implications** – the paper provides relevant self- sufficiency order of magnitude for energy communities. **Originality/value** – our article combines various approaches in a single use case: environmental consideration, behavioural response as well as multi-year energy system sizing.

Keywords: Behavioural response, Greenhouse gas emissions, Optimisation, Self-sufficiency, Energy communities, Energy sufficiency.

# Participate
- Install `co2sizing` using `pip install -e .` in the root folder.
- Test module `pytest tests`
- Be creative :smile:.
- Propose new models in `co2sizing/models/mymodel.py`.
`
