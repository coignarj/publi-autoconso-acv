import pandas as pd
import numpy as np
import plotly.express as px

px.defaults.color_discrete_sequence = px.colors.qualitative.T10


def post_process(model, data, period_coefs, yearly_cons_mwh, verbose=True):
    result = {}
    result = _get_pyomo_timeseries(result, model, [d.index for d in data])
    result["yearly_cons_mwh"] = yearly_cons_mwh
    result["nb_years"] = model.years.data()[-1] + 1

    # Self-sufficiency
    result["self_sufficiency"] = model.self_sufficiency.value * 100

    # Total carbon emissions
    timestep = 15 / 60
    nb_years = result["nb_years"]
    result["co2_kg_mwh_not_discounted"] = np.round(model.co2_emission.value / (yearly_cons_mwh * nb_years), 2)
    result["co2_kg_not_discounted"] = np.round(model.co2_emission.value, 2)

    # Total carbon emissions counting PV export as negative
    discounted_pv = sum(
        sum(
            period_coefs[p]
            * sum(model.pv_curtailed[y, p, t].value * data[p]["co2_kg_kwh"][t] * timestep for t in model.times)
            for p in model.periods
        )
        for y in model.years
    )
    result["co2_kg_mwh"] = np.round((model.co2_emission.value - discounted_pv) / (yearly_cons_mwh * nb_years), 2)
    result["co2_kg"] = np.round(model.co2_emission.value - discounted_pv, 2)

    # Battery and PV sizes
    result["pv_capacity"] = np.round(model.pv_capacity.value, 2)
    result["pv_capacity_yearlymwh"] = np.round(model.pv_capacity.value / yearly_cons_mwh, 2)

    df = pd.DataFrame(index=["_"], data=getattr(model, "battery_cap").get_values())
    df = df.transpose()
    result["battery_cap"] = df["_"].to_list()
    result["battery_kwh"] = [np.round(result["battery_cap"][year], 2) for year in [0, 8, 16]]
    result["battery_kwh_yearlymwh"] = [np.round(result["battery_cap"][year] / yearly_cons_mwh, 2) for year in [0, 8, 16]]

    # CAPEX side of the emissions
    result["capex"] = np.round(
        (
            model.pv_gwp.value * model.pv_capacity.value
            + model.battery_gwp
            * sum([result["battery_cap"][0] + result["battery_cap"][1] + result["battery_cap"][2] / 2])
        ), 2
    )
    result["capex_yearlymwh"] = round(result["capex"] / yearly_cons_mwh)

    # OPEX
    # OPEX = co2_kg_not_discounted - capex

    if verbose:
        print(f'Self-sufficiency   = {result["self_sufficiency"]} %')
        print(f'CO2 emission       = {result["co2_kg_mwh_not_discounted"]} kgCO2eq/MWh')
        print(f'CO2 emission disc. = {result["co2_kg_mwh"]} kgCO2eq/MWh')
        print(f'PV capacity        = {result["pv_capacity_yearlymwh"]} kWp/yearlyMWh')
        print(f'Battery capacity   = {result["battery_kwh_yearlymwh"]} kWh/yearlyMWh')
        print(f'CO2 CAPEX          = {result["capex"]} kgCO2eq')
        print(f'CO2 OPEX           = {result["co2_kg_not_discounted"] - result["capex"]} kgCO2eq')
    return result

def post_process_keys():
    return ["yearly_cons_mwh", "nb_years", "co2_kg_mwh_not_discounted",
            "co2_kg_not_discounted", "co2_kg_mwh", "co2_kg", "pv_capacity", "pv_capacity_yearlymwh",
            "battery_kwh", "battery_kwh_yearlymwh", "capex", "capex_yearlymwh"]

def _get_pyomo_timeseries(result, model, indexes):
    """Custom function to retrive optimization timeseries from `base.py`"""
    keys = ["gridimport", "pv_curtailed", "batteryin", "batteryout", "batteryenergy"]
    nb_years = model.years.data()[-1] + 1
    nb_periods = model.periods.data()[-1] + 1

    for key in keys:
        df = pd.DataFrame(index=["_"], data=getattr(model, key).get_values())
        df = df.transpose()
        df.columns = [key]
        result[key] = []
        for year in range(0, nb_years):
            result[key].append([])
            for period in range(0, nb_periods):
                result[key][-1].append(df.loc[year, period, :, :].copy())
                result[key][-1][-1].index = indexes[period]
    return result


def plot_results(results, data, layout, year=0):
    """Custom function to plot optimization variables from `base.py`"""
    year = 0
    figs = []
    for index in range(0, len(data)):
        fig = px.line(results["gridimport"][year][index])
        fig.data[0].update({"line": {"color": "#d62728"}, "name": "grid_kw", "legendgroup": 2})

        trace = px.line(results["pv_curtailed"][year][index]).data[0]
        fig.add_trace(trace)
        fig.data[1].update({"line": {"color": "#ff7f0e"}, "name": "pv_curtailed", "legendgroup": 1})

        trace = px.line(results["batteryenergy"][year][index]).data[0]
        fig.add_trace(trace)
        fig.data[2].update({"line": {"color": "#2ca02c"}, "name": "battery_kWh", "legendgroup": 2})

        trace = px.line(data[index]["co2_kg_kwh"] * 1000).data[0]
        fig.add_trace(trace)
        fig.data[3].update({"line": {"color": "#bcbd22"}, "name": "co2_g_kwh", "legendgroup": 3})

        trace = px.line(results["batteryin"][year][index]).data[0]
        fig.add_trace(trace)
        fig.data[4].update({"line": {"color": "#e377c2"}, "name": "batteryin", "legendgroup": 4})

        trace = px.line(results["batteryout"][year][index]).data[0]
        fig.add_trace(trace)
        fig.data[5].update({"line": {"color": "#17becf"}, "name": "batteryout", "legendgroup": 5})

        fig.update_layout(layout, showlegend=True, xaxis={"title": ""}, yaxis={"title": "Legend"})
        figs.append(fig)
    return figs
