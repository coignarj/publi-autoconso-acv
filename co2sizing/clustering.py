import numpy as np
import pandas as pd
from scipy.cluster.vq import whiten, vq

from sklearn.metrics import pairwise_distances_argmin_min
from sklearn.cluster import KMeans
from sklearn import preprocessing



class clustering:
    """
    Class for simulation time period reduction using clustering
    (sklearn approach)
    """
    def __init__(self, data):
        """

        :param data: dataframe with at least the following columns
        [ cons_kw , coef_1kw ,   co2_kg_kwh ]
        """
        self.data = data
        self.observation_info = None

    def reshape_data(self, observation_size: int = 7,
                         pre_process: bool = True):

        """
        Method to reshape process data into a numpy array of m * n shape
        :param observation_size: size of each observation (given in days)
        :param pre_process:  Boolean value to determine if the data for clustering should
        undergo pre processing
        :return: numpy array of m * n shape
        """

        if pre_process == True:

            data = preprocessing.minmax_scale(self.data.copy())
            data = pd.DataFrame(data, columns=self.data.columns, index=self.data.index)
        else:
            data = self.data.copy()
        ranges = pd.date_range(start=data.index[0].date(),
                               end=data.index[-1].date(), freq='1D')
        new_data = []
        observation_info = {}
        for i in range(len(ranges) - 1):
            if i + observation_size < len(ranges):
                new_data.append(data.loc[ranges[i]: ranges[i + observation_size]].copy().values)
                observation_info[i] = [ranges[i], ranges[i + observation_size]]
        new_data = np.array(new_data)

        shp = new_data.shape
        new_data = whiten(new_data)
        new_data = new_data.reshape(shp[0], shp[1] * shp[2])

        self.observation_info = observation_info
        # print (new_data.shape)
        return new_data

    def get_weights(self, data, centres, closest, n_clusters):
        """

        :param data: numpy array of m * n shape
        :param centres: list of  index of centroids
        :param closest: list of  index of selected observations
        :param n_clusters: number of clusters
        :return: dictionary of chosen observations and corresponding weights
        """
        coeff_norm = 52 / len(data)
        indx, _ = vq(data, centres)
        indx = np.bincount(indx).tolist()
        weights = {}

        for i in range(n_clusters):
            weights[closest[i]] = coeff_norm * indx[i]

        return weights

    def selected_clusters(self, closest):
        """

        :param closest: list of  index of selected observations
        :return: dataframe of selected observations
        """

        labels = self.observation_info
        df = pd.DataFrame()
        for value in closest:

            temp = self.data.loc[labels[value][0]: labels[value][1]].copy()
            temp["Cluster_number"] = [value] * len(temp)

            if len(df) == 0:

                df = temp
            else:
                df = pd.concat([df, temp])

        return df

    def cluster(self, data, n_clusters):
        """

        :param data: numpy array of m * n shape
        :param n_clusters: number of clusters
        :return: dataframe of selected observations , dictionary of chosen
        observations and corresponding weights
        """

        kmeans = KMeans(n_clusters=n_clusters, init='k-means++',
                        n_init=42, max_iter=300, tol=1e-15).fit(data)
        centres = kmeans.cluster_centers_
        closest = pairwise_distances_argmin_min(centres, data)[0]

        clusters = self.selected_clusters(closest)
        weights = self.get_weights(data, centres, closest, n_clusters)

        return clusters, weights

    def make_pickle(self, df, filepath: str = "../../data/", file_name: str = "clustered_data_DE_.pickle"):
        """

        :param df: dataframe to be pickled
        :param filepath: directory where file is to be stored
        :param file_name: desired file name
        :return:
        """
        df.sort_index().to_pickle(filepath + file_name)

    def make_csv(self, df, filepath: str = "../../data/", file_name: str = "clustered_data_DE_.csv"):
        """

        :param df: dataframe to be pickled
        :param filepath: directory where file is to be stored
        :param file_name: desired file name
        :return:
        """
        df.sort_index().to_csv(filepath + file_name, sep=",", decimal='.', index=True)
