from pyomo.opt import SolverFactory
from pyomo.environ import *
import itertools


def optimize(
    data,
    period_coefs,
    battery_eta,
    pv_gwp,
    battery_gwp,
    positiv_coefs,
    self_sufficiency,
    soh_coefs,
    pv_ageing_coefs,
    solver="glpk",
    verbose=False,
    solver_path=None,
):
    """Optimization model"""
    ################### Model
    m = ConcreteModel()
    timestep = 15 / 60
    nb_years = 20

    ###################################################### Set
    m.years = Set(initialize=list(range(0, nb_years)), ordered=True)
    m.periods = Set(initialize=list(range(0, len(data))), ordered=True)
    m.times = Set(initialize=list(range(0, len(data[0]))), ordered=True)
    last = m.times.last()

    ##################################################### Var
    m.pv_capacity = Var(domain=NonNegativeReals)
    m.battery_cap = Var(m.years, domain=NonNegativeReals)
    m.co2_emission = Var(domain=NonNegativeReals)

    # Equipment specifics
    m.gridimport = Var(m.years, m.periods, m.times, domain=NonNegativeReals)
    m.batteryin = Var(m.years, m.periods, m.times, domain=NonNegativeReals)
    m.batteryout = Var(m.years, m.periods, m.times, domain=NonNegativeReals)
    m.batteryenergy = Var(m.years, m.periods, m.times, domain=NonNegativeReals)
    m.pv_curtailed = Var(m.years, m.periods, m.times, domain=NonNegativeReals)

    #################################################### Param
    m.self_sufficiency = Param(initialize=self_sufficiency)
    m.pv_gwp = Param(initialize=pv_gwp)
    m.battery_gwp = Param(initialize=battery_gwp)
    soh_coefs = list(itertools.chain.from_iterable([soh_coefs for i in range(3)]))

    #################################################### Rules
    # --------------------------------------------------------
    # ---------------------Battery----------------------------
    # --------------------------------------------------------
    def r_battery_max_powerin(m, y, p, t):
        return m.batteryin[y, p, t] <= m.battery_cap[y] / 2

    def r_battery_max_powerout(m, y, p, t):
        return m.batteryout[y, p, t] <= m.battery_cap[y] / 2

    def r_battery_energy(m, y, p, t):
        if t == 0:
            return m.batteryenergy[y, p, t] == m.batteryenergy[y, p, last]
        else:
            return (
                m.batteryenergy[y, p, t]
                == m.batteryenergy[y, p, t - 1]
                + m.batteryin[y, p, t - 1]  # * (1 - 1e-6)+  # avoid oscillations between pv curtailed and battery
                * timestep
                * battery_eta
                - m.batteryout[y, p, t - 1] * timestep / battery_eta
            )

    def r_battery_capacity_yearly(m, y):
        if y == 0:
            return Constraint.Skip
        elif y < 8:
            return m.battery_cap[0] * soh_coefs[y] == m.battery_cap[y]
        elif y == 8:
            return Constraint.Skip
        elif y < 16:
            return m.battery_cap[8] * soh_coefs[y] == m.battery_cap[y]
        elif y == 16:
            return Constraint.Skip
        elif y > 16:
            return m.battery_cap[16] * soh_coefs[y] == m.battery_cap[y]

    def r_battery_min_energy(m, y, p, t):
        return m.batteryenergy[y, p, t] >= m.battery_cap[y] * 0.2

    def r_battery_max_energy(m, y, p, t):
        return m.batteryenergy[y, p, t] <= m.battery_cap[y]

    def r_battery_charge_limit(m, y, p, t):
      return (m.batteryin[y, p, t] <= m.pv_capacity * data[p]['coef_1kw'][t] * pv_ageing_coefs[y])

    # --------------------------------------------------------
    # ------------------Grid imports--------------------------
    # --------------------------------------------------------
    def r_grid_import(m, y, p, t):
        return (
            m.gridimport[y, p, t]
            == positiv_coefs[y] * data[p]["cons_kw"][t]
            - m.pv_capacity * data[p]["coef_1kw"][t] * pv_ageing_coefs[y]
            + m.pv_curtailed[y, p, t]
            + m.batteryin[y, p, t]
            - m.batteryout[y, p, t]
        )

    def r_pv_curtailed(m, y, p, t):
        return m.pv_curtailed[y, p, t] <= m.pv_capacity * data[p]["coef_1kw"][t] * pv_ageing_coefs[y]

    def r_self_suff(m):
        return (
            sum(sum(sum(m.gridimport[y, p, t] for t in m.times) for p in m.periods) for y in m.years)
            / sum(
                sum(sum(positiv_coefs[y] * data[p]["cons_kw"][t] for t in m.times) for p in m.periods) for y in m.years
            )
            == 1 - m.self_sufficiency
        )

    def r_emission(m):
        return (
            m.pv_gwp * m.pv_capacity
            + m.battery_gwp * sum([m.battery_cap[0] + m.battery_cap[8] + m.battery_cap[16] / 2])
            + sum(
                sum(
                    period_coefs[p] * sum(m.gridimport[y, p, t] * data[p]["co2_kg_kwh"][t] * timestep for t in m.times)
                    for p in m.periods
                )
                for y in m.years
            )
        ) == m.co2_emission

    # --------------------------------------------------------
    # ---------------------Add To Model-----------------------
    # --------------------------------------------------------
    # Battery
    m.r1 = Constraint(m.years, m.periods, m.times, rule=r_battery_max_powerin)
    m.r2 = Constraint(m.years, m.periods, m.times, rule=r_battery_max_powerout)
    m.r3 = Constraint(m.years, m.periods, m.times, rule=r_battery_energy)
    m.r4 = Constraint(m.years, m.periods, m.times, rule=r_battery_min_energy)
    m.r5 = Constraint(m.years, m.periods, m.times, rule=r_battery_max_energy)
    m.r6 = Constraint(m.years, m.periods, m.times, rule=r_battery_charge_limit)
    m.r7 = Constraint(m.years, rule=r_battery_capacity_yearly)

    # Grid imports
    m.r8 = Constraint(m.years, m.periods, m.times, rule=r_grid_import)
    m.r9 = Constraint(m.years, m.periods, m.times, rule=r_pv_curtailed)
    m.r10 = Constraint(rule=r_self_suff)
    m.r11 = Constraint(rule=r_emission)

    ##################################################### Objective function
    # Linear objective function
    def objective_function(m):
        return m.co2_emission

    m.objective = Objective(rule=objective_function, sense=minimize)

    #################################################### Run
    # Solve optimization problem
    with SolverFactory(solver, executable=solver_path) as opt:
        results = opt.solve(m, tee=False)
        if verbose:
            print(results)
    return m
